import Mail from '../lib/Mail';

export default {
  key: 'RegistrationMail',
  async handle({ data }) {
    const { user } = data;
    const mailOptions = {
      from: 'Queue test <queue@test.com>',
      to: `${user.name} <${user.email}>`,
      subject: 'QUEUE | Cadastro de usuário',
      html: `Olá, ${user.name}. Bem-vindo ao sistema de filas!`
    };

    await Mail.sendMail(mailOptions);
  }
};
