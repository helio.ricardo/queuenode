REQUIREMENTS
--------------
- Redis running
    docker run --name redis -p 6379:6379 -d -t redis:alpine  
- SMTP Server
    https://mailtrap.io

INSTALL
-------
- yarn
- Duplacate .env.sample to .env and add the configuration
- yarn dev
